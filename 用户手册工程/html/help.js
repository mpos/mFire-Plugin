function copyToClipboard(element)
{ 
var d=document.getElementById(element).innerText;
while(d.indexOf("&amp;")!=-1)
{
d=d.replace("&amp;","&")
}
while(d.indexOf("&lt;")!=-1)
{
d=d.replace("&lt;","<")
}

while(d.indexOf("&gt;")!=-1)
{
d=d.replace("&gt;",">")
}
  if((clipboardData.setData('text', d)))
  alert("已复制到剪贴板");
  else 
  alert("复制失败,请手工复制");  
}



// Q Language 语法高亮
PR.registerLangHandler(PR.createSimpleLexer(
[
["pln",/^[\t\n\r \xA0\u2028\u2029]+/,null,"\t\n\r \u00a0\u2028\u2029"],
["str",/^(?:[\"\u201C\u201D](?:[^\"\u201C\u201D]|[\"\u201C\u201D]{2})(?:[\"\u201C\u201D]c|$)|[\"\u201C\u201D](?:[^\"\u201C\u201D]|[\"\u201C\u201D]{2})*(?:[\"\u201C\u201D]|$))/i,null,'"\u201c\u201d'],
["com",/^[\'\u2018\u2019](?:_(?:\r\n?|[^\r]?)|[^\r\n_\u2028\u2029])*/,null,"'\u2018\u2019"]
],
[
["kwd",/^(?:And|Or|Now|Xor|Mod|Declare|Dim|ReDim|As|ByRef|ByVal|Byte|Short|Integer|Long|Boolean|String|Array|Object|Variant|Class|Namespace|Date|Time|Single|Double|Call|Return|Error|Import|New|COM|Private|Public|Stop|Do|While|Until|Loop|For|in|Each|To|Step|Next|If|Else|ElseIf|EndIf|End|Exit|Select|Case|Is|On|Sub|Function|Event|KeyPress|KeyGroup|KeyDown|KeyUp|KeyWait|KeyState|GetLastKey|SetSimMode|SayString|MouseClick|MouseLeftClick|MouseRightClick|MouseMiddleClick|MouseDoubleClick|MouseDown|MouseLeftDown|MouseRightDown|MouseUp|MouseLeftUp|MouseRightUp|MouseMove|MouseWheel|MouseLock|MouseUnLock|MouseWait|GetCursorPos|GetCursorShape|GetLastClick|GetPixelColor|FindColor|FindMultiColor|CompColor|CompMultiColor|CountColor|FindPic|SnapShot|CacheBegin|CacheEnd|CacheCount|CacheSave|CacheProc|CacheGetPixel|CacheSetPixel|CachePitch|CachePixAddr|CachePixSize|CacheWidth|CacheHeight|RGB|RGBA|HSV|ColorToRGB|ColorToHSV|ColorDiff|SetDict|UseDict|FindStr|Ocr|BindWindow|GetMacroID|ResPath|ExePath|ThreadCreate|ThreadPause|ThreadResume|ThreadStop|ThreadWait|GetThreadID|MutexCreate|MutexFree|LockCreate|LockFree|LockOn|LockOff|Delay|Msgbox|InputBox|RunApp|TracePrint|Iif|Format|CurDir|ChDir|Type|TypeName|CLng|CBool|CDbl|CStr|CDate|IsNull|IsNumeric|IsObject|IsDate|IsArray|IsTable|Abs|Atn|Cos|Exp|Fix|Int|Log|Sgn|Sin|Rnd|Rand|Round|Sqr|Tan|Asc|Chr|Hex|Oct|InStr|InStrRev|LCase|UCase|Left|Right|Mid|Len|LenB|LTrim|RTrim|Trim|Space|String|StrComp|StrReverse|Replace|Join|Split|Array|Filter|UBound|Erase|Now|Timer|Year|Month|MonthName|Day|Weekday|WeekdayName|Hour|Minute|Second|DateAdd|DateDiff|DateValue|TimeValue|DateSerial|TimeSerial|DatePart)\b/i,null],
["com",/^REM\b[^\r\n\u2028\u2029]*/i],
["lit",/^(?:True\b|False\b|Nothing\b|\d+(?:E[+\-]?\d+[FRD]?|[FRDSIL])?|(?:&H[0-9A-F]+|&O[0-7]+)[SIL]?|\d*\.\d+(?:E[+\-]?\d+)?[FRD]?|#\s+(?:\d+[\-\/]\d+[\-\/]\d+(?:\s+\d+:\d+(?::\d+)?(\s*(?:AM|PM))?)?|\d+:\d+(?::\d+)?(\s*(?:AM|PM))?)\s+#)/i],
["pln",/^(?:(?:[a-z]|_\w)\w*(?:\[[%&@!#]+\])?|\[(?:[a-z]|_\w)\w*\])/i],
["pun",/^[^\w\t\n\r \"\'\[\]\xA0\u2018\u2019\u201C\u201D\u2028\u2029]+/],
["pun",/^(?:\[|\])/]]),["vb","vbs","ql"]);
