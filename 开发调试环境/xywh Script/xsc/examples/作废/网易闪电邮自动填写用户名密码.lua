loadplugin("lib\\window.dll",0)
loadplugin("lib\\keyboard.dll",0)
loadplugin("lib\\mouse.dll",0)


-- 函数功能 : 自动填写闪电邮用户名密码
-- 函数参数 :	usertext [需要填写的用户名]
--				usertext [需要填写的密码]
--				debugmode[输出调试信息]
function setmailwindow(usertext,pwtext,debugmode)
	ox,oy = mouse.getpos()
	a = findwindow("新建邮箱帐户")
	window.setpos(a,-1)
	if debugmode then
		print("MainWindow HWND : ",a)
	end
	user = window.getdlgitem(a,1002)
	if debugmode then
		print("UserWindow HWND : ",user)
	end
	pw = window.getdlgitem(a,1004)
	if debugmode then
		print("PassWordWindow HWND : ",pw)
	end
	x,y,w,h = window.getwindowrect(user)
	if debugmode then
		print("UserWindow RECT : ",x,y,w,h)
	end
	x = x+(w-x)/2
	y = y+(h-y)/2
	if debugmode then
		print("MouseMove Point : ",x,y)
	end
	mouse.move(x,y)
	mouse.click(0)
	sleep(50)
	mouse.click(0)
	setcliptext(usertext)
	sleep(100)
	keyboard.down(162)
	keyboard.down(86)
	keyboard.up(86)
	keyboard.up(162)
	x,y,w,h = window.getwindowrect(pw)
	if debugmode then
		print("PassWordWindow RECT : ",x,y,w,h)
	end
	x = x+(w-x)/2
	y = y+(h-y)/2
	if debugmode then
		print("MouseMove Point : ",x,y)
	end
	mouse.move(x,y)
	mouse.click(0)
	sleep(50)
	mouse.click(0)
	setcliptext(pwtext)
	sleep(100)
	keyboard.down(162)
	keyboard.down(86)
	keyboard.up(86)
	keyboard.up(162)
	mouse.move(ox,oy)
end



function printredtxt()
	console.cls()
	print("")
	print("控制说明：")
	print("F9  : 启动脚本[一次]")
	print("F10 : 退出脚本")
	print("")
	print("说明：启动脚本后每隔五秒就会自动点击一下鼠标左键")
	print("并且将复制的内容粘贴并发送出去，可以用于挂机时喊话出售摊位的东西")
	print("在启动脚本之前，请确认鼠标是放在输入框上面的，因为脚本只是点击后粘贴")
	print("脚本会自动在末尾附加字符防止重复判定")
	print("")
	print("想要添加功能，可以打开[编辑器]目录")
	print("用 [xywhAutoGhost v2.exe] 编辑 [AutoSay.lua]")
	print("来实现，软件的右侧是一些常用的功能")
	print("")
	print("")
	print("鄙视垃圾360杀毒软件乱报")
	print("好心被人当木马，xywhAutoGhost是一个开源软件")
	print("源代码下载 : http://www.x64asm.com/AsmBbs/thread-1332-1-1.html")
	print("")
	print("")
	print("作者 : 上杉谦信 [叶子的离开]")
	print("QQ   : 保密")
	print("YY   : 17753688")
	print("")
end

printredtxt()
i = 0
idx = 0
while i==0 do
	S = keyboard.getstate(120)			-- 取F9/F11状态
	E = keyboard.getstate(121)
	if E ~= 0 then
		i = 1
	end
	if S == 0 then
		sleep(200)
	else
		setmailwindow("xywhsoft@qq.com","yefei111111",false)
		sleep(200)
	end
end